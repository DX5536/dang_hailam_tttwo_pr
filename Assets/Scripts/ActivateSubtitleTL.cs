using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace PR_TTTWO
{
    public class ActivateSubtitleTL : MonoBehaviour
    {
        //which director is it
        [SerializeField]
        private PlayableDirector subtitleDirector;

        private string playerTag = "Player";

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(playerTag))
            {
                subtitleDirector.Play();
            }
        }

        //Exit the collider and deactivate the entire Timeline
        private void OnTriggerExit(Collider other)
        {
            subtitleDirector.enabled = false;
        }
    }
}

