using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace PR_TTTWO
{
    public class CameraTransitionPriority : MonoBehaviour
    {
        private string playerTag = "Player";
        [SerializeField]
        private float timer00, timer01, timer02;

        [SerializeField]
        private CinemachineVirtualCamera virtualCam00, virtualCam01, virtualCam02, virtualCam03;

        [SerializeField]
        private float transitionTime;

        //trasition activation
        [SerializeField]
        private bool transitionActive;

        private void Awake()
        {
            timer00 = transitionTime;
        }

        private void Update()
        {
            //constant check when the player collide with collider
            if (transitionActive)
            {
                TransitioningTheCameras();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(playerTag))
            {
                //00 -> 01
                virtualCam01.Priority = virtualCam00.Priority + 5;
                transitionActive = true; //transitioning activating -> starts chain reaction
            }
        }

        private void TransitioningTheCameras()
        {
            timer00 -= Time.deltaTime;

            //01-> 02
            if (timer00 <= 0)
            {
                virtualCam02.Priority = virtualCam01.Priority + 5;
                timer01 -= Time.deltaTime;
            }

            //02 -> 03
            if (timer01 <= 0)
            {
                virtualCam03.Priority = virtualCam02.Priority + 5;
                timer02 -= Time.deltaTime;
            }


            //03 -> 00
            if (timer02 <= 0)
            {
                virtualCam00.Priority = virtualCam03.Priority + 5;
            }
        }
    }

}
